<a  class="topbutton" href="">
    <i class="fa fa-chevron-up" aria-hidden="true"></i>
</a>
<div class="footer-style">
    <footer class="container">
        <div class="site-info">
            <?php
            do_action('lifestyle_credits');
            ?>
            <hr class="hr-style" width="70%">

            <div class="footer-copyright">
                <p class="text-center"> © <?php the_time('Y'); ?> <b class="text-center text-uppercase"><a
                            href="<?php echo get_home_url(); ?>"><?php bloginfo('name') ?></a></b>. All right reserved.


                </p>

            </div>
            <?php my_social_icons_output(); ?>
            <?php wp_footer(); ?>
        </div><!-- .site-info -->
    </footer><!-- .site-footer -->
</div>
</body>
</html>









