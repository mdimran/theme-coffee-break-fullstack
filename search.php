<?php get_header(); ?>
<div class="ls-header">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php if (have_posts()) :
					while (have_posts()) : the_post();
						get_template_part('template-parts/content-single', 'single');
					endwhile;
				endif;
				?>
			</div>
			<div class="col-md-4">
				<div class="ls-sidebar-design">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
