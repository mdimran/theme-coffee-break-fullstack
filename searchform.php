<form role="search" method="get" action="<?php echo esc_url(home_url('/')) ?>">


    <input type="Search" class="form-control" placeholder="Search" value="<?php echo get_search_query() ?>" name="s"
           title="Search"/>


    <button class="search-submit" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>


</form>



