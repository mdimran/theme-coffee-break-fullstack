<article id="post-<?php the_ID(); ?>"<?php post_class(); ?>>

    <div class="single-grid">
        <div class="row">
            <?php if (has_post_thumbnail()): ?>
                <div class="col-md-12">
                    <div class="img-responsive img-style"><?php the_post_thumbnail(); ?></div>
                    <header class="">
                        <?php the_title(sprintf('<h1 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>
                    </header>
                    <?php get_template_part('template-parts/post-meta'); ?>

                    <div class=" text-justify content-font ">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php else: ?>

            <?php endif ?>
        </div>
    </div>

<!--    markup-->
<!--    <div class="single">-->
<!--        <div class="container">-->
<!--            <div class="single-top">-->
<!--                <a href="#"><img class="img-responsive" src="images/single-1.jpg" alt=" "></a>-->
<!--                <div class=" single-grid">-->
<!--                    <h4>SED LOREET ALIQUAM LEOTELLUS DOLOR DAPIBUS</h4>-->
<!--                    <ul class="blog-ic">-->
<!--                        <li><a href="#"><span> <i  class="glyphicon glyphicon-user"> </i>Super user</span> </a> </li>-->
<!--                        <li><span><i class="glyphicon glyphicon-time"> </i>June 14, 2013</span></li>-->
<!--                        <li><span><i class="glyphicon glyphicon-eye-open"> </i>Hits:145</span></li>-->
<!--                    </ul>-->
<!--                    <p>Cras consequat iaculis lorem,</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->



</article>