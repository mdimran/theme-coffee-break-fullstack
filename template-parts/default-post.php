<article id="post-<?php the_ID(); ?>"<?php post_class(); ?> xmlns="http://www.w3.org/1999/html">


    <!--    <div class="row">-->
    <?php if (has_post_thumbnail()): ?>
        <div class="about-main">
            <div class=" about-left">
                <div class="about-tre">
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="<?php the_permalink() ?>"> <?php the_post_thumbnail(); ?></a>
                            <h6><?php the_category(' '); ?></h6>
                            <?php the_title(sprintf('<h3 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h3>'); ?>
                            <p><?php echo wp_trim_words(get_the_content(), 20) ?></p>
                            <label><?php echo the_time('jS F, Y') ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>

        <div class="about-main">
            <div class=" about-left">
                <div class="about-tre">
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <h6><?php the_category(' '); ?></h6>
                            <?php the_title(sprintf('<h3 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h3>'); ?>
                            <p><?php echo wp_trim_words(get_the_content(), 20) ?></p>
                            <label><?php echo the_time('jS F, Y') ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
    <!--    </div>-->

</article>


