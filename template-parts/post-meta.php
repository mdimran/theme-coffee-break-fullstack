<ul class="list-inline post-entry-meta meta-padding post-padding-bottom">
    <li class="posted-by meta-style user-img">
        <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 16); ?> <?php the_author(); ?></a>
    </li>

    <li class="posted-on meta-style">
        <a href="<?php the_permalink() ?>"> <?php echo the_time('jS F, Y')?>
        </a>
    </li>
    <li class="post-comment meta-style">
        <?php setPostViews(get_the_ID()); ?>
        <i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp;Hits:<?php  echo getPostViews(get_the_ID()); ?>
    </li>
</ul>
