<div class="container">
    <div class="about-main">
        <div class="col-md-8 about-left">
            <div class="about-one">
                <p><?php the_category(' '); ?></p>

                <?php the_title(sprintf('<h3 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h3>'); ?>

            </div>
            <div class="about-two">
                <a href="<?php the_permalink() ?>"> <?php the_post_thumbnail(); ?></a>

                <p>Posted by <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php the_author(); ?></a>
                    on <?php echo the_time('jS F, Y') ?> <a href="<?php the_permalink() ?>"><?php comments_number( 'no comments', 'one comment', '% comments' ); ?></a></p>

                <p> <?php echo wp_trim_words(get_the_content(), 20) ?></p>

                <div class="about-btn">
                    <a href="<?php the_permalink() ?>">Read more</a>
                </div>



                <ul>
                    <li><p>Share : </p></li>
                    <li><a href=' http://www.facebook.com/sharer.php?url=" . <?php the_permalink(); ?> . " ' target='_blank'><span <i class="fb fa fa-facebook"></i> </span></a></li>
                    <li><a href='https://twitter.com/share?url=" . <?php the_permalink(); ?> . " ' target='_blank'><span <i class="twit fa fa-twitter"></i> </span></a></li>
                    <li><a href='https://plus.google.com/share?url=" . <?php the_permalink(); ?> . "' target='_blank' ><span <i class="pin fa fa-google-plus"></i> </span></a></li>
                    <li><a href='http://www.tumblr.com/share/link?url=" . <?php the_permalink(); ?> . "' target='_blank'><span <i class="rss fa fa-tumblr"></i> </span></a></li>
                    <!--                    <li><a href="#"><span class="drbl"> </span></a></li>-->
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


