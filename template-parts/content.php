<article id="post---><?php //the_ID(); ?><!--" --><?php //post_class(); ?>
    <?php
    if ( is_sticky(get_the_ID()) ) :
        get_template_part( 'template-parts/sticky-post' );
    else :
        get_template_part( 'template-parts/default-post' );
    endif;
    ?>

</article>
