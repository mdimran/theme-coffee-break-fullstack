<?php get_header(); ?>
    <div class="ls-header">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php if (have_posts()) :
                        while (have_posts()) : the_post();
                            get_template_part('template-parts/content', get_post_format());
                        endwhile;

                        if (story_option('blog-page-nav1', false, true)) :
                            story_posts_pagination();
                        else :
                            story_posts_navigation();
                        endif;
                    endif;
                    ?>
                </div>
                <div class="col-md-4">
                    <div class="ls-sidebar-design">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid no-padding">
        <div class="posts-slider">
            <?php
            do_action("after_listing_posts");
            ?>
        </div><!--.posts-slider-->
    </div>
<?php get_footer(); ?>