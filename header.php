<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title> <?php bloginfo('name'); ?> </title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>



<div class="ls-logo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php
                $custom_logo_id = get_theme_mod('custom_logo');
                $image = wp_get_attachment_image_src($custom_logo_id, 'full');
                ?>
                <a href="<?php echo get_home_url(); ?>">
                    <img class="site-logo" src="<?php echo $image[0]; ?>" alt="not found">
                </a>
            </div>
        </div>
    </div>
</div>





<div class="header ">
    <div class="container">
        <div class="head">
            <div class="row">
                <div class="col-md-6">
                    <div class="navigation">
                        <span class="menu "></span>

                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'container' => 'false',
                                'menu_class' => 'navig'
                            )
                        );
                        ?>
                    </div>
                </div>
                <div class="col-md-3 header-right">
                    <?php get_search_form(); ?>
                </div>
                <div class="col-md-3 header-right">
                    <?php my_social_icons_output(); ?>
<!--                    <ul>-->
<!--                        <span><a href=' http://www.facebook.com/sharer.php?url=" . --><?php //the_permalink(); ?><!-- . " ' target='_blank'><span <i class="fb fa fa-facebook"></i> </span></a></span>-->
<!--                        <span><a href='https://twitter.com/share?url=" . --><?php //the_permalink(); ?><!-- . " ' target='_blank'><span <i class="twit fa fa-twitter"></i> </span></a></span>-->
<!--                        <span><a href='https://plus.google.com/share?url=" . --><?php //the_permalink(); ?><!-- . "' target='_blank' ><span <i class="pin fa fa-google-plus"></i> </span></a></span>-->
<!--                        <span><a href='http://www.tumblr.com/share/link?url=" . --><?php //the_permalink(); ?><!-- . "' target='_blank'><span <i class="rss fa fa-tumblr"></i> </span></a></span>-->
                        <!--                    <li><a href="#"><span class="drbl"> </span></a></li>-->
<!--                    </ul>-->

                </div>

            </div>
        </div>
    </div>
</div>



<!--<div class="header-right">-->
<!--    <div class="search-bar">-->
<!--        --><?php //get_search_form(); ?>
<!--    </div>-->
<!--    --><?php //my_social_icons_output(); ?>
<!--</div>-->




<div class="banner" style="padding-bottom: 60px">
    <div class="container">
        <div class="banner-top">
            <img class="img-responsive" src="<?php header_image(); ?>" alt="<?php echo(get_bloginfo('title')); ?>"/>

            <div class="banner-text">
                <?php
                if ( is_sticky(get_the_ID()) ) :?>
                    <h2><?php the_category(' '); ?></h2>
                    <?php the_title(sprintf('<h1 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>
                    <?php
                endif;
                ?>
                <div class="banner-btn">
                    <a href="<?php the_permalink() ?>">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>

