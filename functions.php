<?php

require get_template_directory() . "/inc/helper.php";
require get_template_directory() . "/inc/template-tags.php";
require get_template_directory() . "/inc/widget/class-wp-widget-lifestyle-recent-posts.php";
require get_template_directory() . "/inc/widget/address-widget.php";
require get_template_directory() . "/inc/widget/popular-post-widget.php";
require get_template_directory() . "/lifestyle-social.php";
require get_template_directory() . "/template-parts/social-icon-show.php";
require get_template_directory() . "/template-parts/post-view count.php";


//include enqueue
function lifestyle_script_enqueue()
{

    wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0', 'all');

    wp_enqueue_style('custom-style-2', get_template_directory_uri() . '/css/coffee-break.css', array(), '1.0', 'all');
    wp_enqueue_style('custom-style', get_template_directory_uri() . '/css/lifestyle.css', array(), '1.1.0', 'all');
    wp_enqueue_style('custom-style-1', get_template_directory_uri() . '/p-css/master-preset.css', array(), '1.1.0', 'all');

    /*===owl carosoul===*/
    wp_enqueue_style('ps-ba-owl-carosoul', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '1.0.0');

    /*===owl carosoul Theme===*/
    wp_enqueue_style('ps-ba-owl-carosoul-theme', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), '1.0.0');


    wp_enqueue_script('jQuery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '3.2.1', true);
    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
    wp_enqueue_script('custom-script', get_template_directory_uri() . '/js/lifestyle.js', array(), '1.0', true);
    // Load owl Js.
    wp_enqueue_script('ps-ba-owl-carosoul', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0.0', TRUE);
    wp_enqueue_script('ps-ba-owl-carosoul-script', get_template_directory_uri() . '/js/owl-script.js', array('jquery'), '1.0.0', TRUE);


}

add_action('wp_enqueue_scripts', 'lifestyle_script_enqueue');


//custom menu
function lifestyle_custom_menu()
{
    add_theme_support('menus');
    register_nav_menu('primary', 'Header Nav');
    register_nav_menu('secondary', 'Footer Nav');
}

add_action('init', 'lifestyle_custom_menu');

//theme support
add_theme_support('html5', array('search-form'));
//add_image_size('ls-img-design', 750, 384, TRUE);
add_theme_support('custom-logo');
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
    'gallery',
    'audio',
));

//side-bar

function lifestyle_widgets_setup()
{
    register_sidebar(array(
        'name' => ('Blog Sidebar'),
        'id' => 'sidebar-1',
        'description' => ('Add widgets here to appear in your sidebar on blog posts and archive pages.'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));

}

add_action('widgets_init', 'lifestyle_widgets_setup');

//for popular post
function wpb_set_post_views($postID)
{
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views($post_id)
{
    if (!is_single()) return;
    if (empty ($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}

add_action('wp_head', 'wpb_track_post_views');


//site logo

function m1_customize_register($wp_customize)
{

    $wp_customize->add_setting('m1_logo'); // Add setting for logo uploader

    // Add control for logo uploader (actual uploader)

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'm1_logo', array(

        'label' => __('Upload 2nd Logo (replaces text)', 'm1'),

        'section' => 'title_tagline',

        'settings' => 'm1_logo',

    )));

}

add_action('customize_register', 'm1_customize_register');


//for slider

add_action('after_listing_posts', 'implement_posts_slider');

function implement_posts_slider()
{
    get_template_part('template-parts/posts-slider');
}

add_image_size('bon-appetit-related-post-thumbnail', 270, 155, array('center', 'center'));


if (!function_exists('coffeebreak_comment_form')) :

    function coffeebreak_comment_form()
    {
        global $user_identity;
        $commenter = wp_get_current_commenter();
        $req = get_option('require_name_email');
        $aria_req = ($req ? " aria-required='true'" : '');

        if (comments_open()) { ?>
            <div id="respond">
            <form id="comment-form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">

            <?php if (is_user_logged_in()) : ?>

                <div id="comment-input" class="row">
                    <div class="col-md-12">
                        <p><?php esc_html_e('Logged in as', 'coffeebreak'); ?> <a
                                href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.
                            <a href="<?php echo wp_logout_url(get_permalink()); ?>"
                               title="Log out of this account"><?php esc_html_e('Log out &raquo;', 'coffeebreak'); ?></a>
                        </p>
                    </div>

                    <div class="col-md-8 comment-bottom heading ">
                        <h3 for="comment" class="comment-placeholder placeholder"><?php esc_html_e('Comment', 'coffeebreak'); ?></h3>
                        <div id="comment-textarea" class="placeholding-input form-style">

                                <textarea placeholder="<?php esc_html_e('Comment', 'coffeebreak'); ?>" name="comment"
                                          id="comment" cols="60" rows="8" tabindex="4"
                                          class="textarea-comment logged-in form-control"></textarea>
                        </div>
                        <div class="form-submit form-button-padding">
                            <input class="btn btn-primary btn-lg" name="submit" type="submit" id="submit"
                                   value="Post Comment">
                            <?php comment_id_fields(); ?>
                            <?php do_action('comment_form', get_the_ID()); ?>
                        </div>
                    </div>
                </div>

            <?php else : ?>


                <div class="single">
<!--                    <div class="container">-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="single-top">
                                    <div class="comment-bottom heading">
                                        <h3 for="comment"> <?php esc_html_e('Leave a Comment', 'coffeebreak'); ?> </h3>

                                        <div class="form-style">

                                            <div class="">
                                                <input placeholder="<?php esc_html_e('Name', 'coffeebreak'); ?>"
                                                       type="text" name="author" id="author"
                                                       tabindex="1" <?php if ($req) echo "aria-required='true'"; ?>
                                                       class="form-control input-name"/>
                                            </div>
                                            <div class="">
                                                <input placeholder="<?php esc_html_e('Email', 'coffeebreak'); ?>"
                                                       type="text" name="email" id="email"
                                                       tabindex="2" <?php if ($req) echo "aria-required='true'"; ?>
                                                       class="form-control input-email"/>
                                            </div>
                                            <div class="">
                                                <input placeholder="<?php esc_html_e('Subject', 'coffeebreak'); ?>"
                                                       type="text" name="url" id="url" tabindex="3"
                                                       class="form-control input-website"/>
                                            </div>
                                            <div id="comment-textarea" class="">
                                                <div class="placeholding-input">
                                                        <textarea
                                                            placeholder="<?php esc_html_e('Comment', 'coffeebreak'); ?>"
                                                            name="comment" id="comment" cols="60" rows="8" tabindex="4"
                                                            class="textarea-comment form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-submit form-button-padding ">
                                                <input class="btn btn-primary btn-lg send-btn-style" name="submit" type="submit"
                                                       id="submit" value="Send">
                                                <?php comment_id_fields(); ?>
                                                <?php do_action('comment_form', get_the_ID()); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--                        </div>-->
                    </div>
                </div>

            <?php endif; ?>

            <?php
        }
    }
endif; ?>


<?php


if (!function_exists("coffeebreak_comments")) :

    function coffeebreak_comments($comment, $args, $depth)
    {

        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) {

            // Display trackbacks differently than normal comments.
            case 'pingback' :
            case 'trackback' :
                ?>

                <div <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                <p><?php esc_html_e('Pingback:', 'coffeebreak'); ?><?php comment_author_link(); ?><?php edit_comment_link(esc_html__('(Edit)', 'coffeebreak'), '<span class="edit-link">', '</span>'); ?></p>

                <?php
                break;

            default :
                // Proceed with normal comments.
                global $post;
                ?>
            <div <?php comment_class(); ?> id="div-comment-<?php comment_ID(); ?>">
                <div id="comment-<?php comment_ID(); ?>" class="comment media ">
                    <div class="media-body comment-author clearfix media-heading">


                        <div class="media-right avatar-size">
                            <a href="<?php echo get_comment_author_link(); ?>">
                                <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, 20); ?>
                            </a>
                        </div>


                        <div class="media-body">
                            <h4 class="media-heading">
                                <?php echo get_comment_author(); ?>
                            </h4>

                            <?php if ('0' == $comment->comment_approved) { //Comment moderation ?>
                                <div class="alert alert-info">
                                    <?php esc_html_e('Your comment is awaiting moderation.', 'coffeebreak'); ?>
                                </div>
                            <?php } ?>

                            <div class="comment-content"">
                                <?php comment_text(); //Comment text
                                ?>
                                <?php comment_reply_link(array_merge($args, array(
                                    'reply_text' => esc_html__('REPLY', 'sygnus'),
                                    'before' => '<span class="reply">',
                                    'after' => '</span>',
                                    'depth' => $depth,
                                    'max_depth' => $args['max_depth']
                                ))); ?>
                            </div>
                            <!-- .comment-content -->
                        </div>
                        </div>
                    </div>
                </div>
                <!-- #comment-## -->
                <?php
                break;
        } // end comment_type check

    }

endif;



