<?php get_header(); ?>
<div class="ls-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (have_posts()) :
                    while (have_posts()) : the_post();
                        get_template_part('template-parts/content-single', 'single');
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
