<?php

//----------------------------------------------------------------------
// Theme Option Name
//----------------------------------------------------------------------

if (!function_exists('story_option_name')):
    function story_option_name()
    {

        if (function_exists('pstudio_plugin_theme_option_name')) {
            return pstudio_plugin_theme_option_name();
        }

        return apply_filters('pstudio_theme_option_name', 'pstudio_theme_option');
    }
endif;

//----------------------------------------------------------------------
// Getting Theme Option data
//----------------------------------------------------------------------

if (!function_exists('story_option')):
    function story_option($index = FALSE, $index2 = FALSE, $default = NULL)
    {

        if (function_exists('pstudio_plugin_theme_option')) {
            return pstudio_plugin_theme_option($index, $index2, $default);
        }

        $story_theme_option_name = story_option_name();

        if (!isset($GLOBALS[$story_theme_option_name])) {
            return $default;
        }

        $story_theme_option = $GLOBALS[$story_theme_option_name];

        if (empty($index)) {
            return $story_theme_option;
        }

        if ($index2) {
            $result = (isset($story_theme_option[$index]) and isset($story_theme_option[$index][$index2])) ? $story_theme_option[$index][$index2] : $default;
        } else {
            $result = isset($story_theme_option[$index]) ? $story_theme_option[$index] : $default;
        }

        if ($result == '1' or $result == '0') {
            return $result;
        }

        if (is_string($result) and empty($result)) {
            return $default;
        }

        return $result;
    }
endif;

?>