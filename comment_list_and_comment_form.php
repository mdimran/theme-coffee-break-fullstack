<?php 
//----------------------------------------------------------------------
/* Get comment form */
//----------------------------------------------------------------------
if ( ! function_exists( 'story_comment_form' ) ) :

function story_comment_form(){
	global $user_identity;
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	if ( comments_open() ) { ?>
		<div id="respond">
        	<div class="comment-title col-md-6 padding-left">
            	<h4><?php comment_form_title(__('Leave A Comment', 'story'), __('Leave A Comment', 'story')); ?></h4>
        	</div>
            <div class="col-md-6 cancel-reply padding-left"><?php cancel_comment_reply_link(); ?></div>
			<div class="col-md-12 padding-left"><p class="ps-mailthread"><?php esc_html_e('Your email address will not be published. * Required fields are marked *', 'story'); ?></div>
			<form id="comment-form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
        	
            <?php if ( is_user_logged_in() ) : ?>

			<div id="comment-input" class="row">
				<div class="col-md-12 col-sm-12"><p><?php esc_html_e('Logged in as', 'story'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account"><?php esc_html_e('Log out &raquo;', 'story'); ?></a></p></div>
				<div class="col-md-12 col-sm-12">
					<div id="comment-textarea" class="placeholding-input">
						<label for="comment" class="comment-placeholder placeholder"><?php esc_html_e('Comment', 'story'); ?></label>
						<textarea placeholder="<?php esc_html_e('Comment', 'story'); ?>" name="comment" id="comment" cols="60" rows="8" tabindex="4" class="textarea-comment logged-in form-control"></textarea>
					</div>
					<div class="form-submit">
						<input class="btn btn-primary btn-lg" name="submit" type="submit" id="submit" value="Post Comment">
						<?php comment_id_fields(); ?>
						<?php do_action('comment_form', get_the_ID()); ?>
					</div>
				</div>	
			</div>	
		
		<?php else : ?>
		
			<div id="comment-input" class="row">
				<div id="comment-textarea" class="col-md-12 col-sm-12">
					<div class="placeholding-input">
						<label for="comment" class="comment-placeholder placeholder"><?php esc_html_e('Comment', 'story'); ?></label>
						<textarea placeholder="<?php esc_html_e('Comment', 'story'); ?>" name="comment" id="comment" cols="60" rows="8" tabindex="4" class="textarea-comment form-control"></textarea>
					</div>
				</div>				
				<div class="col-sm-4 col-md-4 form-group ps-formtag-style">
					<label for="author" class="placeholder"><?php esc_html_e('Name', 'story'); ?></label>
					<span class="required star-style">*</span>
					<input type="text" name="author" id="author" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> class="form-control input-name" />
				</div>
				<div class="col-sm-4 col-md-4 form-group ps-formtag-style">
					<label for="email" class="placeholder"><?php esc_html_e('Email', 'story'); ?></label>
					<span class="required star-style">*</span>
					<input type="text" name="email" id="email" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> class="form-control input-email"  />
				</div>
				<div class="col-sm-4 col-md-4 form-group ps-formtag-style">
					<label for="url" class="placeholder"><?php esc_html_e('Website', 'story'); ?></label>
					<input type="text" name="url" id="url" tabindex="3" class="form-control input-website" />
				</div>
			</div>
			<div class="form-allowed-tags">
				<h5 class="ps-single-endtag"><?php esc_html_e( 'You may use these', 'story')?> <abbr title="HyperText Markup Language"><?php esc_html_e( 'HTML', 'story'); ?></abbr> <?php esc_html_e( 'tags and attributes:', 'story'); ?></h5>
				<code> <?php echo allowed_tags(); ?> </code>
			</div>
            <div class="form-submit">
                <input class="btn btn-primary btn-lg" name="submit" type="submit" id="submit" value="Post Comment">
                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', get_the_ID()); ?>
            </div>
    
            <?php endif; ?>
        </form>
    
	<?php
	}
}
endif;

//----------------------------------------------------------------------
// Comments list
//----------------------------------------------------------------------

if ( ! function_exists( "story_comments" ) ) :

	function story_comments( $comment, $args, $depth ) {

		$GLOBALS[ 'comment' ] = $comment;
		switch ( $comment->comment_type ) {

			// Display trackbacks differently than normal comments.
			case 'pingback' :
			case 'trackback' :
				?>

				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
				<p><?php esc_html_e( 'Pingback:', 'story' ); ?><?php comment_author_link(); ?><?php edit_comment_link( esc_html__( '(Edit)', 'story' ), '<span class="edit-link">', '</span>' ); ?></p>

				<?php
				break;

			default :
				// Proceed with normal comments.
				global $post;
				?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
				<div id="comment-<?php comment_ID(); ?>" class="comment media">
					<div class="comment-author clearfix">
						<div class="media-left">
						<?php if ($args['avatar_size'] != 0) echo  get_avatar( $comment, 80 ); ?>
						</div>
						<div class="media-body">
							<div class="comment-meta media-heading">
								<h4>
									<span class="author-name">
										<?php echo get_comment_author(); ?>
									</span>
								</h4>
								<span>
									<span class="comment-date"><?php echo get_comment_date(' jS \of F Y'); ?><?php echo " at ".get_comment_time(); ?></span>
								</span>
								<div class="comment-meta">
									<?php edit_comment_link( esc_html__( 'Edit', 'story' ), '<span class="edit-link">', '</span>' ); //edit link
									?>
									<?php comment_reply_link( array_merge( $args, array(
										'reply_text' => '<i class="fa fa-reply"></i><span class="reply">' . esc_html__( 'Reply', 'story' ) . '</span>',
										'depth'      => $depth,
										'max_depth'  => $args[ 'max_depth' ]
									) ) ); ?>

								</div>

							</div>

							<?php if ( '0' == $comment->comment_approved ) { //Comment moderation ?>
								<div class="alert alert-info">
									<?php esc_html_e( 'Your comment is awaiting moderation.', 'story' ); ?>
								</div>
							<?php } ?>

							<div class="comment-content">
								<?php comment_text(); //Comment text
								?>
							</div>
							<!-- .comment-content -->
						</div>
					</div>
				</div>
				<!-- #comment-## -->
				<?php
				break;
		} // end comment_type check

	}

endif;