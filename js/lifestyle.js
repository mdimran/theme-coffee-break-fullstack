/**
 * Created by Mohammad on 6/14/2017.
 */


jQuery(document).ready(function(){
    $(".show-icons").hide();
    $(".hide-icons").on("click", function(){
    	$(this).next().show();
    	$(this).hide();

    });
});



jQuery(document).ready(function($){
    var offset = 100;
    var speed = 600;
    var duration = 500;
    $(window).scroll(function(){
        if ($(this).scrollTop() < offset) {
            $('.topbutton') .fadeOut(duration);
        } else {
            $('.topbutton') .fadeIn(duration);
        }
    });
    $('.topbutton').on('click', function(){
        $('html, body').animate({scrollTop:0}, speed);
        return false;
    });
});


$("span.menu").click(function(){
    $(" ul.navig").slideToggle("slow" , function(){
    });
});







//// ================================================================
//// Drop Down Menu
//// ================================================================
//
//(function () {
//
//    function getIEVersion() {
//        var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
//        return match ? parseInt(match[1]) : false;
//    }
//
//    if (getIEVersion()) {
//        $('html').addClass('ie ie' + getIEVersion());
//    }
//
//    if ($('html').hasClass('ie9') || $('html').hasClass('ie10')) {
//        $('.submenu-wrapper').each(function () {
//            $(this).addClass('no-pointer-events');
//        });
//    }
//
//
//    var timer;
//
//    $('li.dropdown').on('mouseenter', function (event) {
//
//
//        event.stopImmediatePropagation();
//        event.stopPropagation();
//
//        $(this).removeClass('open menu-animating').addClass('menu-animating');
//        var that = this;
//
//        if (timer) {
//            clearTimeout(timer);
//            timer = null;
//        }
//
//        timer = setTimeout(function () {
//
//            $(that).removeClass('menu-animating');
//            $(that).addClass('open');
//
//        }, 300);   // 300ms as css animation end time
//
//    });
//
//    // on mouse leave
//
//    $('li.dropdown').on('mouseleave', function (event) {
//
//        var that = this;
//
//        $(this).removeClass('open menu-animating').addClass('menu-animating');
//
//
//        if (timer) {
//            clearTimeout(timer);
//            timer = null;
//        }
//
//        timer = setTimeout(function () {
//
//            $(that).removeClass('menu-animating');
//            $(that).removeClass('open');
//
//        }, 300);  // 300ms as animation end time
//    });
//}());